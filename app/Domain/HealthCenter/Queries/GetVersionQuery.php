<?php

namespace Domain\HealthCenter\Queries;

use Domain\HealthCenter\DataTransferObjects\InfraVersionDto;
use Support\Concerns\IQuery;

class GetVersionQuery implements IQuery
{

    public function __invoke(): InfraVersionDto
    {
        return new InfraVersionDto(
            laravelVersion: app()->version(),
            phpVersion: phpversion()
        );
    }

}

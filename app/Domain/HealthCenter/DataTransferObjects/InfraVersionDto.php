<?php

namespace Domain\HealthCenter\DataTransferObjects;

use Spatie\LaravelData\Data;

class InfraVersionDto extends Data
{
    public function __construct(
        public string $laravelVersion,
        public string $phpVersion
    )
    {
    }

}

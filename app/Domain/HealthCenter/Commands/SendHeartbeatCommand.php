<?php

namespace Domain\HealthCenter\Commands;

use Http;
use Log;
use Support\Concerns\ICommand;

class SendHeartbeatCommand implements ICommand
{

    public function __invoke(): void
    {
        // do not use in local env
        if (app()->isLocal()) {
            return;
        }
        $url = config('services.uptime_robot.url');
        if (!empty($url)) {
            $res = Http::post($url);
            if ($res->failed()) {
                Log::warning("Failed to send heartbeat: Request to $url failed");
            }
        } else {
            Log::error("Failed to send heartbeat,no url");
        }
    }
}

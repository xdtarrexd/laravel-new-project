<?php

namespace Support\Concerns;

interface IQuery
{

    public function __invoke();

}

<?php

namespace Support\Concerns;

interface ICommand
{
    public function __invoke(): void;
}

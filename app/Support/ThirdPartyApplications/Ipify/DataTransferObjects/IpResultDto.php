<?php

namespace Support\ThirdPartyApplications\Ipify\DataTransferObjects;

use Spatie\LaravelData\Data;

class IpResultDto extends Data
{
    public function __construct(
        public string $ipAddress
    )
    {
    }

}

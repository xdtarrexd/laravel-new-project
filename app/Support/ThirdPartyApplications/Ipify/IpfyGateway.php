<?php

namespace Support\ThirdPartyApplications\Ipify;

use Http;
use Support\ThirdPartyApplications\Ipify\DataTransferObjects\IpResultDto;

class IpfyGateway
{
    public static function GetIpV4Address(): IpResultDto
    {
        $res = Http::retry(2)->get('https://api.ipify.org?format=json');

        return new IpResultDto(
            ipAddress: $res->json('ip')
        );
    }

}

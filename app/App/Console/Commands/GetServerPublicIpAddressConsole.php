<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Support\ThirdPartyApplications\Ipify\IpfyGateway;

class GetServerPublicIpAddressConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:get-server-public-ip-address';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $ip = IpfyGateway::GetIpV4Address();
        $this->info("Resolved IP: $ip->ipAddress");
    }
}

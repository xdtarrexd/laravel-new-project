<?php

namespace App\Console\Commands;

use Domain\HealthCenter\Queries\GetVersionQuery;
use Illuminate\Console\Command;

class GetVersionConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:version-checker';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check version';

    /**
     * Execute the console command.
     */
    public function handle(GetVersionQuery $query)
    {
        $versions = $query();

        $this->info("Laravel version: " . $versions->laravelVersion);
        $this->info("Php version: " . $versions->phpVersion);
    }
}

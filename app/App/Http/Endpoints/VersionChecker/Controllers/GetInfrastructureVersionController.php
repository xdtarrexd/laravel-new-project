<?php

namespace App\Http\Endpoints\VersionChecker\Controllers;

use Domain\HealthCenter\Queries\GetVersionQuery;

class GetInfrastructureVersionController
{
    public function __invoke(GetVersionQuery $query)
    {
        return $query()->toArray();
    }
}
